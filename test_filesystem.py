import os

import pytest


# ########### UTILS ############
def create_file(filename: str):
    with open(filename, 'w') as f:
        f.write(f'Hello {filename}!')


def delete_file(filename: str):
    os.remove(filename)


def file_exists(filename: str) -> bool:
    return os.path.exists(filename)


# ############ FIXTURES ##########

@pytest.fixture
def create_file_titi():
    # setup
    filename = 'titi.txt'
    create_file(filename)

    yield

    # teardown
    delete_file(filename)


@pytest.fixture
def create_file_with(filename_param):
    # setup
    create_file(filename_param)

    yield

    delete_file(filename_param)


# ############ TESTS ##########

def test_file_exists(create_file_titi):
    assert file_exists('titi.txt') == True


@pytest.mark.parametrize('filename_param', ['file-1', 'file-2'])
def test_file_exists_with_parametrize(create_file_with, filename_param):
    assert file_exists(filename_param) == True
